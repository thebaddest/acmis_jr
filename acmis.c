#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define PROGRAM_LENGTH 5
#define MAX_NAME_LENGTH 51
#define DATE_LENGTH 11
#define REGISTRATION_LENGTH 7

// Define a struct for Student
typedef struct Student
{
    char name[MAX_NAME_LENGTH];
    char regNum[REGISTRATION_LENGTH];
    char programCode[PROGRAM_LENGTH];
    char birthYear[5];
    char birthMonth[3];
    char birthDay[3];
    int tuition;
    struct Student *next;
} template;

// Function to load data from a file into linked list
void loadFromFile(template **head)
{
    template *tail = *head;
    FILE *theFile = fopen("archive.txt", "r");
    if (theFile == NULL)
    {
        return;
    }
    char line[90];
    while (fgets(line, sizeof(line), theFile) != NULL)
    {
        template *node = (template *)malloc(sizeof(template));
        sscanf(line, "%50[^,],%6[^,],%4[^,],%4[^,],%2[^,],%2[^,\n],%d", node->name, node->regNum, node->programCode, node->birthYear, node->birthMonth, node->birthDay, &node->tuition);
        if (tail == NULL)
        {
            *head = node;
            tail = node;
        }
        else
        {
            tail->next = node;
            tail = node;
        }
    }
}

// Function to save data from linked list to a file
void saveToFile(template **head)
{
    template *node = *head;
    FILE *theFile = fopen("archive.txt", "w");
    while (node != NULL)
    {
        fprintf(theFile, "%s,%s,%s,%s,%s,%s,%d\n", node->name, node->regNum, node->programCode, node->birthYear, node->birthMonth, node->birthDay, node->tuition);
        node = node->next;
    }
    fclose(theFile);
    printf("Saved to file\n");
}

// Function to convert string to integer
int wordIntMaker(char *string)
{
    int value = 0;
    for (int i = 0; string[i] != '\0'; i++)
    {
        value += (int)string[i];
    }
    return value;
}

// Function to display all students in the list
void showStudents(template **head)
{
    template *node = *head;
    int counter = 1;
    if (node == NULL)
    {
        printf("No students currently\n");
        return;
    }
    while (node != NULL)
    {
        printf("%d. %s      %s     Reg: %s     DOB: %s-%s-%s     Tuition: Shs.%d\n", counter, node->name, node->programCode, node->regNum, node->birthDay, node->birthMonth, node->birthYear, node->tuition);
        ++counter;
        node = node->next;
    }
}

// Function to sort a list of strings
void listSort(char *lst[], int length)
{
    for (int k = 0; k < length; k++)
    {
        int p = k;
        int lowestIndex = k;
        while (p < length)
        {
            int q = 0;
            while ((int)lst[p][q] != '\0')
            {
                if (lst[p][q] < lst[lowestIndex][q])
                {
                    lowestIndex = p;
                    break;
                }
                else if (lst[p][q] > lst[lowestIndex][q])
                {
                    break;
                }
                ++q;
            }
            p++;
        }
        char *temp = lst[k];
        lst[k] = lst[lowestIndex];
        lst[lowestIndex] = temp;
    }
}

// Function to sort students based on name or registration number
void sortStudents(int *num, template **head)
{
    if (*head == NULL)
    {
        printf("Nothing to sort\n");
        return;
    }
    if (*num != 1 && *num != 2)
    {
        printf("Choice out of range\n");
        return;
    }
    int counter = 1;
    template *node = *head;
    while (node != NULL)
    {
        ++counter;
        node = node->next;
    }
    node = *head;
    char *elements[counter];
    int pos = 0;
    while (node != NULL)
    {
        char *nodeName;
        if (*num == 1)
        {
            nodeName = node->name;
        }
        else
        {
            nodeName = node->regNum;
        }
        elements[pos] = nodeName;
        ++pos;
        node = node->next;
    }
    --counter;
    listSort(elements, counter);
    printf("Sorted list:\n");
    for (int r = 1; r <= counter; r++)
    {
        printf("%d. %s\n", r, elements[r - 1]);
    }
}

// Function to add a new student to the list
void addStudent(template **head)
{
    template *node = *head;
    template *prevNode = NULL;
    template *newNode = (template *)malloc(sizeof(template));
    printf("Enter student's name: ");
    scanf(" %[^\n]", newNode->name);
    printf("Enter student's date of birth (YYYY MM DD): ");
    scanf(" %s %s %s", newNode->birthYear, newNode->birthMonth, newNode->birthDay);
    printf("Enter student's program code (4 characters): ");
    scanf(" %s", newNode->programCode);

    // Validate program code
    // do
    // {
    //     printf("Enter student's program code (4 characters): ");
    //     scanf(" %4s", newNode->programCode);
    //     if (strlen(newNode->programCode) != 4)
    //     {
    //         printf("Please enter a valid program code (4 characters).\n");
    //     }
    // } while (strlen(newNode->programCode) != 4);

    int tuition;
    do
    {
        printf("Enter student's annual tuition (greater than 0): ");
        scanf(" %d", &tuition);
        if (tuition <= 0)
        {
            printf("Please enter a valid tuition (greater than 0).\n");
        }
    } while (tuition <= 0);
    newNode->tuition = tuition;

    int randomRegNum = wordIntMaker(newNode->name);
    char currentRegNum[7];
    snprintf(currentRegNum, 7, "%06d", randomRegNum);
    for (int r = 0; r < 7; r++)
    {
        newNode->regNum[r] = currentRegNum[r];
    }
    if (node == NULL)
    {
        *head = newNode;
    }
    while (node != NULL)
    {
        prevNode = node;
        node = node->next;
    }
    if (prevNode != NULL)
    {
        prevNode->next = newNode;
    }
    printf("Student added.\n");
}

// Function to edit information of a student
void editStudent(int *position, template **head)
{
    template *node = *head;
    template *prevNode = NULL;
    if (*head == NULL)
    {
        printf("No students currently\n");
        return;
    }
    for (int i = 1; i < *position; i++)
    {
        if (node->next != NULL)
        {
            prevNode = node;
            node = node->next;
        }
        else
        {
            printf("Out of range.\n");
            return;
        }
    }
    printf("\nStudent name: %s\n", node->name);
    printf("Registration number (cannot be changed): %s\n", node->regNum);
    printf("Student date of birth: %s-%s-%s\n", node->birthYear, node->birthMonth, node->birthDay);
    printf("Student program: %s\n", node->programCode);
    int choice = 0;
    while (choice != 5)
    {
        printf("\nChoose edit option\n");
        printf("1. Edit name\n");
        printf("2. Edit date of birth\n");
        printf("3. Edit program code\n");
        printf("4. Edit tuition\n");
        printf("5. Back to main menu\n");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
        {
            printf("Enter new name: ");
            scanf(" %[^\n]", node->name);
            printf("Name changed.\n");
            break;
        }
        case 2:
        {
            printf("Enter new date of birth (YYYY MM DD): ");
            scanf(" %s %s %s", node->birthYear, node->birthMonth, node->birthDay);
            printf("Date of birth changed.\n");
            break;
        }
        case 3:
        {
            printf("Enter new program code: ");
            scanf(" %s", node->programCode);
            printf("Program changed.\n");
            break;
        }
        case 4:
        {
            printf("Enter new tuition: ");
            scanf(" %d", &(node->tuition));
            printf("Tuition changed.\n");
            break;
        }
        default:
            printf("Choice: %d", choice);
        }
    }
}

// Function to delete a student from the list
void deleteStudent(int *position, template **head)
{
    template *node = *head;
    template *prevNode = NULL;
    if (*head == NULL)
    {
        printf("No students currently\n");
        return;
    }
    for (int i = 1; i < *position; i++)
    {
        if (node->next != NULL)
        {
            prevNode = node;
            node = node->next;
        }
        else
        {
            printf("Out of range.\n");
            return;
        }
    }
    printf("Student name: %s\n", node->name);
    printf("Student number: %s\n", node->regNum);
    printf("Press 1 to confirm deletion or any other key otherwise:\n");
    int f;
    scanf("%d", &f);
    if (f == 1)
    {
        if (prevNode == NULL)
        {
            *head = node->next;
        }
        else
        {
            prevNode->next = node->next;
        }
        printf("Deleted successfuly\n");
    }
    else
    {
        printf("Aborted.\n");
    }
}

// Function to search for a student by registration number
void search(template **head)
{
    if (*head == NULL)
    {
        printf("No students currently\n");
        return;
    }
    printf("Enter full registration number (6 digits):\n");
    char query[6];
    scanf("%s", query);
    template *node = *head;
    bool foundMatch = false;
    while (node != NULL)
    {
        bool matching = true;
        int r = 0;
        while (node->regNum[r] != '\0')
        {
            if (node->regNum[r] != query[r])
            {
                matching = false;
                break;
            }
            ++r;
        }
        if (matching == true)
        {
            foundMatch = true;
            break;
        }
        node = node->next;
    }
    if (foundMatch == false)
    {
        printf("No match was found for %s.\n", query);
    }
    else
    {
        printf("Found a match: %s\nRegistration number: %s\n", node->name, node->regNum);
    }
}

// Function to get user's choice for sorting
int sortChoice()
{
    printf("Sort by:\n");
    printf("1. Name\n");
    printf("2. Registration number\n");
    int num;
    scanf("%d", &num);
    return num;
}

// Function to get user's choice for position
int position()
{
    printf("Enter position number of student:\n");
    int pos;
    scanf("%d", &pos);
    return pos;
}

void main()
{
    template *temp0 = NULL;
    loadFromFile(&temp0);
    while (1)
    {
        printf("\n\n## Academic Records System ##\n");
        printf("Choose file operation below:\n");
        printf("1. Show enrolled students\n");
        printf("2. Sort students' list\n");
        printf("3. Add student\n");
        printf("4. Edit student information\n");
        printf("5. Delete student\n");
        printf("6. Search by registration number\n");
        printf("7. Save data to file\n");
        printf("8. Exit program\n");
        int choice;
        printf("Enter your choice: ");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
        {
            showStudents(&temp0);
            break;
        }
        case 2:
        {
            int num = sortChoice();
            sortStudents(&num, &temp0);
            break;
        }
        case 3:
        {
            addStudent(&temp0);
            break;
        }
        case 4:
        {
            int pos = position();
            editStudent(&pos, &temp0);
            break;
        }
        case 5:
        {
            int pos = position();
            deleteStudent(&pos, &temp0);
            break;
        }
        case 6:
        {
            search(&temp0);
            break;
        }
        case 7:
        {
            saveToFile(&temp0);
            break;
        }
        case 8:
        {
            printf("Exiting...\n");
            exit(0);
        }
        default:
            printf("Choice out of range\n");
        }
    }
}