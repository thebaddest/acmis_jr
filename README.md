# acmis_jr

## About Project
The application is implemented in C language which is a low-level language and is known for better speed and peformance compared to many higher level languages.


The central object of our records management application is the student information structure which contains various items of different data types namely, the student's name, registration number, program code, date of birth and annual tuition.


We managed to create a connected archive of students' data through implementing linked lists. Our application opens with a command-line interface showing a numbered menu from which the user can select any management activity to perform on student data, for example they can choose to add a new student to the repository, edit a student's information, delete a student, read the list of all students currently stored or save student data to an archive for future use.


Our data is stored in a csv file containing comma-separated values and when the application starts, data which was saved in this file previously is loaded into memory and can be viewed or even edited, according to the system manager's desire.


Our application also implemented sorting of students' records using the selection sort method and we also enable a user to search for a record by a student's registration number. We have implemented all these options through a looping switch-case statement which enables a user to perform multiple actions on student data without the application terminating.


The records management system we have built is therefore a good miniaturised version of a real-world records management system and can be used as a sample for benchmarking on how to design the same functionality on a larger scale. 




# links to 3-min videos
- [Ntonde Edgar Isingoma](https://youtu.be/y6mXorubpmo) 
- [Naggayi Daphne Pearl](https://youtu.be/ILJyM3OOuX0)



## Members of this Project 
- 👤 [NTONDE EDGAR ISINGOMA - 23/U/24810/EVE](https://gitlab.com/eddiisingoma)
- 👤 [NAGGAYI DAPHNE PEARL - 23/U/13097/EVE](https://gitlab.com/daphnepearl101)
- 👤 [ASIINGWIRE ERIC - 23/U/06707/EVE](https://gitlab.com/latimore14)
- 👤 [NABBAALE CLAIRE - 23/U/12770/EVE](https://gitlab.com/claireleah256)
